3/16/23:

TODO (Crockett): get the performance of our kinematic model via comparison of output variables

TODO (Ben): creating a new non-linear model for parameter identification (https://github.com/alexliniger/MPCC)

3/23/23:
TODO (Crockett): 
    
    - meeting with Tim

    - fix the kinematic model --> open-loop using only commanded speed and commanded steering
    
    - plot x and y integrated from the kinematic model and compare the output to the ground truth from vectornav


TODO (Ben):

    - data gathering run simple trajectory at constant velocity -- straight line data gathering

    - fix/evaluate the vectornav acceleration data
        
        -- if we can fix the vectornav acceleration data then re-do data gathering runs
    
    