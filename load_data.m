clear
hold on
w_r = 0.1397; % wheel radius (m)
slip_ratio_min_fv = 0.1; % minimum foward velocity to calculate slip ratio
L = 1.050; % wheelbase [m] 
T = 1.1;   % track width [m]

folderPath = fullfile(pwd,"custom");
%copyfile("simulator_msgs",folderPath);
ros2genmsg(folderPath)

bag = ros2bagreader("rosbags/voltron_modeling");

teensy_odom_msgs = select(bag, "Topic", "topic"); % wheel speed
vectornav = select(bag, "Topic", "vectornav/velocity_body"); % vectornav
vectornav_accel = select(bag, "Topic", "vectornav/imu"); % vectornav imu
vcu_msgs = select(bag, "Topic", "voltron_vcu/status"); % vcu topic



teensy_odom_data = readMessages(teensy_odom_msgs);



vectornav_data = readMessages(vectornav);
vectornav_accel_data = readMessages(vectornav_accel);
vcu_data = readMessages(vcu_msgs);

t1 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), teensy_odom_data);
t2 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), vectornav_data); % joint states: alot more data
t3 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), vcu_data); % joint states: alot more data

k = 1;
if (size(t1,1) < size(t2,1)) & (size(t1,1) < size(t3,1))
    for i = 1:size(t1,1)
        [val,indx] = min(abs(t1(i) - t2));
        [val2,indx2] = min(abs(t1(i) - t3))
        if (val <= 0.1) & (val2 <= 0.1)
            idx(k,:) = [i indx indx2]; % t1, t2, t3
            k = k + 1;
        end
    end
elseif (size(t2,1) < size(t1,1)) & (size(t2,1) < size(t3,1))
    for i = 1:size(t2,1)
        [val,indx] = min(abs(t2(i) - t1));
        [val2,indx2] = min(abs(t2(i) - t3))
        if (val <= 0.1) & (val2 <= 0.1)
            idx(k,:) = [indx i indx2]; % t1, t2, t3
            k = k + 1;
        end
    end
else
    for i = 1:size(t3,1)
        [val,indx] = min(abs(t3(i) - t1));
        [val2,indx2] = min(abs(t3(i) - t2))
        if (val <= 0.1) & (val2 <= 0.1)
            idx(k,:) = [indx indx2 i]; % t1, t2, t3
            k = k + 1;
        end
    end
end

aligned_data = [teensy_odom_data(idx(:,1)), vectornav_data(idx(:,2)), vcu_data(idx(:,3)), vectornav_accel_data(idx(:,2))];



linear_x_vel_vals = cellfun(@(x) x.twist.twist.linear.x, aligned_data(:,2));
lateral_y_accel_vals = cellfun(@(x) x.linear_acceleration.y, aligned_data(:,4));
yaw_vel_vals = cellfun(@(x) x(1).twist.twist.angular.z, aligned_data(:,2));


front_left_av  = cellfun(@(x) (x.fl_wheel_rpm * ((2* w_r *pi)/ 60)), aligned_data(:,1));
front_right_av = cellfun(@(x) (x.fr_wheel_rpm *((2* w_r *pi)/ 60)), aligned_data(:,1));
rear_left_av = cellfun(@(x) ((x.throttle/3) * ((2* w_r *pi)/ 60)), aligned_data(:,3));

steer_angles = cellfun(@(x)  sin(x.steer*0.105)*0.44, aligned_data(:,3)) % TODO prob fix this bad assumption / calculation


for i=1:size(front_left_av)
    if linear_x_vel_vals(i) >= slip_ratio_min_fv
        slip_fl(i) = abs(((front_left_av(i)) / linear_x_vel_vals(i))-1);
        slip_fr(i) = abs(((front_right_av(i)) / linear_x_vel_vals(i))-1);
        slip_rl(i) = abs(((rear_left_av(i)) / linear_x_vel_vals(i))-1);

    else
        slip_fl(i) = 0
        slip_fr(i) = 0
        slip_rl(i) = 0
    end
end

slip_rr = slip_rl




hold on

% plt = plot(slip_fl, 'DisplayName', 'slip fl capped');
% plt2 =plot(linear_x_vel_vals, 'DisplayName', 'linear x vel vals');
% plt3 =plot(front_left_av.*w_r, 'DisplayName', 'front left av');
% legend([plt plt2 plt3],'slip fl','linear x vel vals', 'front left av')
hold off
%slip_fl=slip_fl*0.5
%slip_fr=slip_fr*0.5
%slip_rl=slip_rl*0.5
%slip_rr=slip_rr*0.5

y1 = [linear_x_vel_vals, lateral_y_accel_vals, yaw_vel_vals];
u1 = [slip_fl.', slip_fr.', slip_rl.', slip_rr.', steer_angles];





