import pandas as pd
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter
from scipy.signal import freqz
import numpy as np
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

order = 6
fs = 20.0       # sample rate, Hz
cutoff = 3.5  # desired cutoff frequency of the filter, Hz
# Get the filter coefficients so we can check its frequency response.

df = pd.read_csv("out2.csv")
time = df['timestamps'].tolist()
time_adj = [x/float(1e9) for x in time]
initialtime = float(time_adj[0])
time_adj = [x-initialtime for x in time_adj]




accel_data = df["a_x_arr"]
measured_vel = df["v_x_meas_arr"]

fs = 20.0
low = 0.01
high = 3.0
# filt_data = butter_bandpass_filter(accel_data, low, high, fs, order=3)
y = butter_lowpass_filter(accel_data, cutoff, fs, order=6)
filt_data = []
dv_dt =[]
prev_v=0
for v in df["v_x_meas_arr"].to_list():
    dv = v-prev_v
    prev_v = v
    dv_dt.append(dv/0.05)
    # print(kf.x[0])
    # filt_data.append(kf.x[0][0])

y2 = butter_lowpass_filter(dv_dt, cutoff, fs, order=6)
plt.figure(2)
plt.clf()
# # plt.plot(time_adj, accel_data, label='Noisy signal')
# plt.plot(time_adj, y, label='Filtered signal')
# plt.plot(time_adj, dv_dt, label='dv_dt signal')
plt.plot(time_adj, y2, label='dv_dt signal')
plt.plot(time_adj, measured_vel, label='Filtered signal')
plt.show()
