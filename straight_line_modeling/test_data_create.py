import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
C_m1 = 0.15
C_m2 = -1.24
C_r = 0.0
C_d = 0.1
mass = 145.0

#initializing the changing values
I_in = 0
v_x_meas = 0

dt = 0.01
t=0

t_arr =[]
a_x_arr =[]

I_in_arr=[]
v_x_meas_arr=[]
while(t < 10):
    
    if(I_in < 100.0):
        I_in += (0.01 - ((t/100)*0.01))
    
    if(v_x_meas < 4.5):
        v_x_meas += 0.3
    
    a_x = ((C_m1 - (C_m2 * v_x_meas))*I_in - C_r - C_d*(pow(v_x_meas,2)))/mass
    t += dt
    t_arr.append(t)
    a_x_arr.append(a_x)
    I_in_arr.append(I_in)
    v_x_meas_arr.append(v_x_meas)

d = {'t_arr': t_arr, 'a_x_arr': a_x_arr, 'I_in_arr':I_in_arr, 'v_x_meas_arr':v_x_meas_arr}
df = pd.DataFrame(data=d)
df = df.reset_index()
df.to_csv('out.csv')
print(df)
plt.plot(t_arr, a_x_arr)
plt.show()