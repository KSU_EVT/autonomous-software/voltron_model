import pandas as pd
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter
import numpy as np
mass = 145.0

C_r = 0.0
df = pd.read_csv("out2.csv")
time = df['timestamps'].tolist()

accel_data = df["a_x_arr"]


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

# def fun(theta):
#     return y(theta, ts) - ys

# x, y, z= velocity, current, ref_acceleration
def fun(params, x, y, z):
    C_m1, C_m2, C_d, C_r = params
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    return ((((C_m1 - (C_m2 * x))* y - C_r - C_d*(pow(x,2)))/mass) - z)


order = 6
fs = 20.0       # sample rate, Hz
cutoff = 3.5  # desired cutoff frequency of the filter, Hz
# Get the filter coefficients so we can check its frequency response.

time_adj = [x/float(1e9) for x in time]
initialtime = float(time_adj[0])
time_adj = [x-initialtime for x in time_adj]
measured_vel = df["v_x_meas_arr"]

fs = 20.0
low = 0.01
high = 3.0

filt_data = []
dv_dt =[]
prev_v=0
for v in df["v_x_meas_arr"].to_list():
    dv = v-prev_v
    prev_v = v
    dv_dt.append(dv/0.05)

accel_filtered = butter_lowpass_filter(dv_dt, cutoff, fs, order=6)


x0 = [1.0, -2.0, 1.0, 1.0]

v_in = list(map(float, df["v_x_meas_arr"].to_list()))
I_in = list(map(float, df["I_in_arr"].to_list()))
accel_out = list(map(float, accel_filtered))
result = least_squares(fun, x0, args=(v_in, I_in, accel_out))

measured_vel = df["v_x_meas_arr"].to_list()
I_in = df["I_in_arr"].to_list()

print("C_m1:", result.x[0])
print("C_m2:", result.x[1])
print("C_d:", result.x[2])
print("C_r:", result.x[3])

C_m1= result.x[0]
C_m2= result.x[1]
C_d= result.x[2]
C_r= result.x[3]

ax_est = []
for index in range(len(measured_vel)):
    vel_val =float(measured_vel[index])
    I_in_val = float(I_in[index])
    a_x = ((C_m1 - (C_m2 * vel_val))*I_in_val - C_r - C_d*(pow(v,2)))/mass
    ax_est.append(a_x)

time_adj = [x/float(1e9) for x in time]
initialtime = float(time_adj[0])

time_adj = [x-initialtime for x in time_adj]

plt.plot(time_adj, df['v_x_meas_arr'].tolist(), color='r')
plt.plot(time_adj, accel_filtered, color='y') 
plt.plot(time_adj, ax_est, color='b')


plt.show()
