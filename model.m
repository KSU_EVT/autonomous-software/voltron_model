%load(fullfile(matlabroot, 'toolbox', 'ident', 'iddemos', 'data', 'vehicledata'));
simData = iddata(y1, u1, 0.04, 'Name', 'Simulated high tire stiffness vehicle data');
ModelFile = 'voltron_c'; %model file
Order = [3, 5, 3];
Parameters    = [145.15; 1.2; 0.64; 1.5e4; 2.0e4; 0.1];  % Initial parameters.
InitialStates = [0.4; 0; 0];
Ts = 0;

nlgr = idnlgrey(ModelFile, Order, Parameters, InitialStates, Ts, ...
                'Name', 'Bicycle vehicle model', 'TimeUnit', 's');
        nlgr.InputName =  {'Slip on front left tire';               ...   % u(1).
                         'Slip on front right tire';              ...   % u(2).
                         'Slip on rear left tire';                ...   % u(3).
                         'Slip on rear right tire';               ...   % u(4).
                         'Steering angle'};                       ...   % u(5).
          nlgr.InputUnit =  {'ratio'; 'ratio'; 'ratio'; 'ratio'; 'rad'};

          nlgr.OutputName = {'Long. velocity';  ...   % y(1); Longitudinal vehicle velocity
                         'Lat. accel.';   ...     % y(2); Lateral vehicle acceleration
                         'Yaw rate'};                             ...   % y(3).
          nlgr.OutputUnit = {'m/s'; 'm/s^2'; 'rad/s'};


nlgr = setinit(nlgr, 'Name', {'Longitudinal vehicle velocity' ... % x(1).
                            'Lateral vehicle velocity' ... % x(2).
                        'Yaw rate'}); ... % x(3).
nlgr = setinit(nlgr, 'Unit', {'m/s'; 'm/s'; 'rad/s'});
nlgr.InitialStates(1).Minimum = eps(0); % Longitudinal velocity > 0 for the model to be valid.
nlgr = setpar(nlgr, 'Name', {'Vehicle mass'; ... % m.
                           'Distance from front axle to COG'; ... % a
                           'Distance from rear axle to COG'; ... % b.
                           'Longitudinal tire stiffness'; ... % Cx.
                           'Lateral tire stiffness'; ... % Cy.
                       'Air resistance coefficient'}); ... % CA.
    nlgr = setpar(nlgr, 'Unit', {'kg'; 'm'; 'm'; 'N'; 'N/rad'; '1/m'});
nlgr = setpar(nlgr, 'Minimum', num2cell(eps(0) * ones(6, 1))); % All parameters > 0!


nlgr.Parameters(1).Fixed = true;
%nlgr.Parameters(2).Fixed = true;
%nlgr.Parameters(3).Fixed = true;

simData.InputName = nlgr.InputName;
simData.InputUnit = nlgr.InputUnit;
simData.OutputName = nlgr.OutputName;
simData.OutputUnit = nlgr.OutputUnit;
simData.Tstart = 0;
simData.TimeUnit = 's';
nlgr = nlgreyest(simData, nlgr);



h_gcf = gcf;
set(h_gcf, 'DefaultLegendLocation', 'southeast');
h_gcf.Position = [100 100 795 634];

for i = 1:simData.Nu
    subplot(simData.Nu, 1, i);
    plot(simData.SamplingInstants, simData.InputData(:, i));
    title(['Input #' num2str(i) ': ' simData.InputName{i}]);
    xlabel('');
    axis tight;
end

xlabel([simData.Domain ' (' simData.TimeUnit ')']);

figure(1);

for i = 1:simData.Ny
    subplot(simData.Ny, 1, i);
    plot(simData.SamplingInstants, simData.OutputData(:, i));
    title(['Output #' num2str(i) ': ' simData.OutputName{i}]);
    xlabel('');
    axis tight;
end

xlabel([simData.Domain ' (' simData.TimeUnit ')']);

figure(2);

compare(simData, nlgr, [], compareOptions('InitialCondition', 'model'));
