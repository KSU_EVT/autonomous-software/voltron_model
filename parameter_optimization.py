import numpy as np
from multiprocessing import Process

#read in steering angles, throttle commands, and GT drive distances from csv file
data = np.genfromtxt("mydata.csv", delimiter=",")

timestep = 0.05

VOLTRON_MASS = 150 #? kilos

NUM_PROCS = 11

FORWARD_PREDICTION_STEPS = 20

class Model:
    def __init__(self, C_m1, C_m2, C_r, C_d):
        self.C_m1 = C_m1
        self.C_m2 = C_m2
        self.C_r = C_r
        self.C_d = C_d

    #run forward predictive model
    #@param prediction time = number of steps to forward simulate
    #@commandStates = input commands to kart in ndarray organized as [<drivetrain output torque>, <commanded steering angle>]
    #initCommand = index from which to start reading command states (defaults to 0)
    #initPosition = starting position of the kart in X
    #initVel = starting velocity of the kart in X
    def predict(self, prediction_time, commandStates, initCommand = 0, initPosition = 0, initVel = 0):
        _X = initPosition
        _X_dot = initVel
        for t in range(prediction_time):
            _X += _X_dot
            _X_dot += ((self.C_m1 - self.C_m2*_X_dot)*np.cos(commandStates[1, t+initCommand])*commandStates[0, t+initCommand] - self.C_r - self.C_d*_X_dot**2)/VOLTRON_MASS
        return _X            
    
    def __str__(self):
        return str(self.C_m1) + " " + str(self.C_m2) + " " + str(self.C_r) + " " + str(self.C_d)


#calculate l2 loss over entire model for given forward prediction distance
def loss(model, GT_data, prediction_time=1):
    l2_agg = 0
    index = 0
    for distance in GT_data[prediction_time:]:
        l2_agg += (model.predict(prediction_time, data[0:2, :], initCommand = index)-distance)**2
        index += 1
    return l2_agg, model


def paramSearch(limits = [[0, 0, 0, 0], [1000000, 1000000, 1000000, 1000000]], stepSize = 1000, prevVals = []):
    if len(limits[0])>1:
        minVal = np.inf
        minModel = None
        for i in range(limits[0][0], limits[1][0], stepSize):
            errVal, testModel = paramSearch([limits[0][1:], limits[1][1:]], stepSize, prevVals+[i])
            if errVal < minVal:
                minVal = errVal
                minModel = testModel
        return minVal, minModel
    else:
        minError = np.inf
        minModel = None
        for i in range(limits[0][0], limits[1][0], stepSize):
            testModel = Model(prevVals[0], prevVals[1], prevVals[2], i)
            testError = loss(Model, data[2, :], FORWARD_PREDICTION_STEPS)
            if testError < minError:
                minError = testError
                minModel = testModel
        return minError, minModel

def honedSearch(start = 100000, end = 0.01, decayRate = 0.01):
    iterStep = start
    lim_Cm1 = [0, 1000000]
    lim_Cm2 = [0, 1000000]
    lim_Cr = [0, 1000000]
    lim_Cd = [0, 1000000]
    while timestep > end:
        err, model = paramSearch(limits = [[lim_Cm1[0], lim_Cm2[0], lim_Cr[0], lim_Cd[0]], [lim_Cm1[1], lim_Cm2[1], lim_Cr[1], lim_Cd[1]]], stepSize = iterStep)
        lim_Cm1 = [model.C_m1-iterStep, model.C_m1+iterStep]
        lim_Cm2 = [model.C_m2-iterStep, model.C_m2+iterStep]
        lim_Cr = [model.C_r-iterStep, model.C_r+iterStep]
        lim_Cd = [model.C_d-iterStep, model.C_d+iterStep]
        iterStep *= decayRate
    print(model)
    print(err)

if __name__ == "__main__":
    honedSearch()
