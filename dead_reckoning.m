clear
w_r = 0.1397; % wheel radius (m)
slip_ratio_min_fv = 0.1; % minimum foward velocity to calculate slip ratio
L = 1.050; % wheelbase [m] 
T = 1.1;   % track width [m]

folderPath = fullfile(pwd,"custom");
%copyfile("simulator_msgs",folderPath);
ros2genmsg(folderPath)

bag = ros2bagreader("rosbags/voltron_modeling");

teensy_odom_msgs = select(bag, "Topic", "topic"); % wheel speed
vectornav = select(bag, "Topic", "vectornav/velocity_body"); % vectornav
vectornav_accel = select(bag, "Topic", "vectornav/imu"); % vectornav imu
vcu_msgs = select(bag, "Topic", "voltron_vcu/status"); % vcu topic
position_msgs = select(bag, "Topic", "vectornav/pose");



teensy_odom_data = readMessages(teensy_odom_msgs);



vectornav_data = readMessages(vectornav);
vectornav_accel_data = readMessages(vectornav_accel);
vcu_data = readMessages(vcu_msgs);
position_data = readMessages(position_msgs);

t1 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), teensy_odom_data);
t2 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), vectornav_data); % joint states: alot more data
t3 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), vcu_data); % joint states: alot more data
t4 = cellfun(@(x) double(x.header.stamp.sec) + (double(x.header.stamp.nanosec) * 1e-9), position_data); % joint states: alot more data


k = 1;
if (size(t1,1) < size(t2,1)) & (size(t1,1) < size(t3,1) & (size(t1, 1) < size(t4,1)))
    for i = 1:size(t1,1)
        [val,indx] = min(abs(t1(i) - t2));
        [val2,indx2] = min(abs(t1(i) - t3));
        [val3,indx3] = min(abs(t1(i) - t4));
        if (val <= 0.1) & (val2 <= 0.1) & (val3 <= 0.1)
            idx(k,:) = [i indx indx2 indx3]; % t1, t2, t3, t4
            k = k + 1;
        end
    end
elseif (size(t2,1) < size(t1,1)) & (size(t2,1) < size(t3,1) & (size(t2, 1) < size(t4,1)))
    for i = 1:size(t2,1)
        [val,indx] = min(abs(t2(i) - t1));
        [val2,indx2] = min(abs(t2(i) - t3));
        [val3,indx3] = min(abs(t2(i) - t4));
        if (val <= 0.1) & (val2 <= 0.1) & (val3 <= 0.1)
            idx(k,:) = [indx i indx2 indx3]; % t1, t2, t3
            k = k + 1;
        end
    end
elseif ((size(t3, 1) < size(t1, 1)) & (size(t3,1) < size(t2,1)) & (size(t3,1) < size(t4, 1)))
    for i = 1:size(t3,1)
        [val,indx] = min(abs(t3(i) - t1));
        [val2,indx2] = min(abs(t3(i) - t2));
        [val3,indx3] = min(abs(t3(i) - t4));
        if (val <= 0.1) & (val2 <= 0.1) & (val3 <= 0.1)
            idx(k,:) = [indx indx2 i indx3]; % t1, t2, t3
            k = k + 1;
        end
    end
else
    for i = 1:size(t4,1)
        [val,indx] = min(abs(t4(i) - t1));
        [val2,indx2] = min(abs(t4(i) - t2));
        [val3,indx3] = min(abs(t4(i) - t3));
        if (val <= 0.1) & (val2 <= 0.1) & (val3 <= 0.1)
            idx(k,:) = [indx indx2 indx3 i]; % t1, t2, t3
            k = k + 1;
        end
    end
end

aligned_data = [teensy_odom_data(idx(:,1)), vectornav_data(idx(:,2)), vcu_data(idx(:,3)), vectornav_accel_data(idx(:,2)), position_data(idx(:,4))];



linear_x_vel_vals = cellfun(@(x) x.twist.twist.linear.x, aligned_data(:,2));
linear_y_vel_vals = cellfun(@(x) x.twist.twist.linear.y, aligned_data(:,2));
yaw_vel_vals = cellfun(@(x) x(1).twist.twist.angular.z, aligned_data(:,2));
steer_angles = cellfun(@(x) x.steer, aligned_data(:, 3));
rpm_commands = cellfun(@(x) x.throttle, aligned_data(:, 3))*0.958/3/60;
absolute_x_vals = cellfun(@(x) x.pose.pose.position.x, aligned_data(:,5));
absolute_y_vals = cellfun(@(x) x.pose.pose.position.y, aligned_data(:,5));
quatx = cellfun(@(x) x.pose.pose.orientation.x, aligned_data(:,5));
quaty = cellfun(@(x) x.pose.pose.orientation.y, aligned_data(:,5));
quatz = cellfun(@(x) x.pose.pose.orientation.z, aligned_data(:,5));
quatw = cellfun(@(x) x.pose.pose.orientation.w, aligned_data(:,5));
absolute_quat = [quatw, quatx, quaty, quatz];

absolute_x_vals = absolute_x_vals - absolute_x_vals(1);
absolute_y_vals = absolute_y_vals - absolute_y_vals(1);
[absolute_yaw, absolute_pitch, absolute_roll] = quat2angle(absolute_quat, "ZYX");
absolute_yaw = -absolute_yaw;

integrated_x_vals(1) = 0;
integrated_y_vals(1) = 0;
integrated_yaw_vals(1) = absolute_yaw(1);
for i=2:size(absolute_x_vals,1)
    integrated_x_vals(i) = integrated_x_vals(i-1) + rpm_commands(i)*cos(integrated_yaw_vals(i-1)+pi/2)*0.05;% + linear_y_vel_vals(i)*sin(integrated_yaw_vals(i-1)+pi/2)*0.05;
    integrated_y_vals(i) = integrated_y_vals(i-1) - rpm_commands(i)*sin(integrated_yaw_vals(i-1)+pi/2)*0.05;% + linear_y_vel_vals(i)*cos(integrated_yaw_vals(i-1)+pi/2)*0.05;
    integrated_yaw_vals(i) = integrated_yaw_vals(i-1) + linear_x_vel_vals(i)*steer_angles(i)*0.6/13*0.05;%yaw_vel_vals(i)*0.05;
end

integrated_yaw_vals=wrapToPi(integrated_yaw_vals) ;
for i=1:3
    subplot(3, 1, i)
    hold on
    if i == 1
        plot(1:size(absolute_x_vals), absolute_x_vals)
        plot(1:size(absolute_x_vals), integrated_x_vals)
        title("X axis, actual vs predictions")
    elseif i == 2
        plot(1:size(absolute_x_vals), absolute_y_vals)
        plot(1:size(absolute_x_vals), integrated_y_vals)
        title("Y axis, actual vs predictions")
    else
        plot(1:size(absolute_x_vals), absolute_yaw)
        plot(1:size(absolute_x_vals), integrated_yaw_vals)
        title("Yaw, actual version predictions")
    end
    hold off
    
end