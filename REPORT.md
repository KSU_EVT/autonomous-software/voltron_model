overleaf file for the report itself:

https://www.overleaf.com/8463943864rhfcdjnsqpdz

outline:

- motivation

- relevant literature 

- system setup
    
    -- sensors used

    -- state variables measured

    -- outputs

- modeling and simulation

    -- steering model identification experimental method used

        --- comparison to other methods used

    -- kinematic model used for comparison 

    -- dynamic model identified

    
